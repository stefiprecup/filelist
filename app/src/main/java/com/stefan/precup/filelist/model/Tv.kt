package com.stefan.precup.filelist.model

data class Tv(
    val episode: Int,
    val season: Int
)