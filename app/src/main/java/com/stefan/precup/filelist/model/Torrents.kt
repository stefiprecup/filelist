package com.stefan.precup.filelist.model

import androidx.room.Entity

@Entity(tableName = "torrents")
data class Torrents(
    val category: String,
    val comments: Int,
    val download_link: String,
    val files: Int,
    val freeleech: Int,
    val id: Int,
    val imdb: String,
    val `internal`: Int,
    val leechers: Int,
    val moderated: Int,
    val name: String,
    val seeders: Int,
    val size: Int,
    val small_description: String,
    val times_completed: Int,
    val tv: Tv,
    val upload_date: String
)